<?php

$firstname = 'farzad';
$_firstname = 'farzad';
//$0firstname     غلط
//$!firstname     غلط
//$firstname0     درست
//$firstname!     غلط
//$firstname $Firstname فرق دارد

/*

1 - حروف کوچک و بزرگ در تعریف نام متغییر مهم هست
2 - نمیتوان در اول نام متغییر از عدد استفاده کرد
3 - از کاراکتر های غیر متنی نباید استفاده کرد
4 - فقط میتوان از آندرلاین استفاده کرد


*/

//echo $firstname;


$num1 = 10;
$num2 = 20;
$num3 = 30;


//echo "$num1 * $num2";
echo $num1 * $num2;


?>